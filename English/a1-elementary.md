# 01. Saying Hello

![Alphabet](https://bytebucket.org/ilyaboltarev/info/raw/83dd5488c013d76a91232ce56923c584f6771d20/English/images/01-01-1.jpg)

![Numbers](https://bytebucket.org/ilyaboltarev/info/raw/5045a38f9f5cf12b5e131a9a3bb17e9c0ad18a9c/English/images/01-01-2.jpg)

# 02. Let's get acquainted

## Theory

I am sick - я болен  
I sick - я натравливаю собаку  

Обязательные элементы - кто и что делает  
В I am sick добавляем am (be), чтоб было "Я являюсь больным" (кто и что делает)

### Отрицание:

I am not a teacher - я не учитель  
They are not at home - Они не дома

I + am (not)  
He, she, it + is (not)  
We, you, they + are (not)

### Personal pronouns (личные местоимения):

I, he, she, it, we, you, they - личные местоимения  
My friend = he + is  
My friends = they + are

С everybody применяем is  
Everybody is at home - все дома

### Short forms (сокращения):

I am (not) = I'm | I'm not  
He, she, it is (not) = he's, she's, it's | he isn't, she isn't, it isn't  
We are (not) = we're, you're, they're | we aren't

![To Be Present](https://bytebucket.org/ilyaboltarev/info/raw/5045a38f9f5cf12b5e131a9a3bb17e9c0ad18a9c/English/images/01-02.jpg)

# 03. Meeting people

## Theory

### Глагол be: are you? is he? are they?

Что делает + кто = ?  
Am I?  
Are you / we / they?  
Is he / she / it?

Am I young?  
Is she at home?

### Are you? - Yes, I am / No, I am not

Are you? - Yes, I am / No, I am not  
Why are you happy?

### Articles a/an

Article + имя существительное (noun)  
A/an = один человек или предмет  
I am a teacher  
Sam is an optician  
They are teachers

### Articles: исключения

Be + a good teacher / an old optician  
He is good (нет существительного - нет артикля)  
He is a good doctor (есть существительное - ставим артикль)  
He is a judge  
He is an old judge

![To Be Present: question](https://bytebucket.org/ilyaboltarev/info/raw/5045a38f9f5cf12b5e131a9a3bb17e9c0ad18a9c/English/images/01-03.jpg)

# 04. Family and friends

## Dictionary

nephew - племянник  
niece - племянница  
tired - уставший  
cousin - кузен  
busy - занятой

## Theory

### Plural form (множественное число)

What is it? Who is it?  
Noun + s = множественное число  
A city - cities (t, b, s + y - ies)  
A boy - boys (a, e, o, u + y - ys)  
A bus - buses (s, ss, sh, ch, x, o - es)  
A knife - knives (f, fe - ves), но chief - chiefs, roof - roofs

### Nouns: артикли и множественное число

A friend - friends  
An apple - apples

### Possessives (показатели принадлежности)

Possessive adjectives: my, his, her, your, their, our, its  
Possessive case: my friend's, my friends'

His books are quite interesting  
His friend's books are also interesting - Книги его друга тоже интересные  
But her friends' books are boring - А вот книги ее друзей скучные

### This/these или that/those: то или это?

This/these - этот/эти, вот здесь  
That/those - тот/те, вон там  
This is my dress - вот это мое платье  
That is my dress - вон то мое платье  
These are our coats - вот это наши пальто  
Those are our coats - вон то наши пальто

a man - men  
a woman - women  
a child - children  
a mouse - mice  
a tooth - teeth  
a foot - feet  
a deer - deer  
a fish - fish

![Plural form: endings](https://bytebucket.org/ilyaboltarev/info/raw/5045a38f9f5cf12b5e131a9a3bb17e9c0ad18a9c/English/images/01-04.jpg)

# 05. Same or different

## Theory

### Глагол have: I have / He has

Глагол be: am, is, are - есть, быть, являться  
Глагол have: have, has - иметь, владеть, обладать  
I / we / you / they have - у меня есть  
He / she / it has - у него есть

### Nouns: article a/an или some

Если предметы можно посчитать, то говорим как обычно:  
I have 10 dollars  
Если предмет исчисляемый и он один, то можно добавить a:  
We have a coin  
Если предмет исчисляемый и его несколько, то можно добавить some:  
Sean has some ties  
Если предмет неисчисляемый, употребляем some:  
I have some water  
Susie has some money  
They have some paper  
Неисчисляемые существительные во множественном числе не употребляются

Если предмет исчисляемый или неисчисляемый и его много, то надо ставить a lot of:  
They have a lot of dictionaries  
My friends have a lot of water

### Articles: a/an, the

A/an - indefinite article = one, упоминается впервые  
The - definite article = this, уже известный предмет, упоминается повторно  
Артикль A употребляется когда предмет один и о нем мы упоминаем впервые  
They have a lamp  
А дальше продолжаем (второе предложение)  
The lamp is bright (конкретная лампа, уже известная нам по первому предложению, яркая)

### Proper names and articles - артикли с именами собственными

Proper names - имена собственные  
Name, Surname, City, Country, Day of the week, Month, Holiday - proper names. Пишутся с заглавной буквы  
Ann is always sick in December  
У собственных имен не ставится артикль, но есть исключения:  
The USA, the United Kingdom, the Czech Republic

![To Have Present: affirmative](https://bytebucket.org/ilyaboltarev/info/raw/5045a38f9f5cf12b5e131a9a3bb17e9c0ad18a9c/English/images/01-05.jpg)

# 06. Food you have

## Dictionary

bread - хлеб  
butter - масло  
cucumber - огурец  
cake - торт  
oil - растительное масло  
sausage - колбаса  
plum - слива  
grapes - виноград  
cabbage - капуста  
pear - груша

## Theory

### Глагол have: I do not have / He does not have

Have - иметь, обладать, владеть  
Глагол have: I have, he has - у меня есть, у него есть  
They have a fridge  
He has a mixer  
We do not have a fridge  
And she does not have a mixer

### Глагол have: Do you have? - Yes, I do / No, I do not

Do I / you / we /they have? - Yes, I do / No, we do not  
Does he / she / it have? - Yes, he does / No, she does not  
Do not = don't  
Does not = doesn't  
Do you have a fridge?  
Does your brother have a suit?  
Do you have a cake? - Yes, I do.  
Does Dylan have a melon? - No, he does not

### Количество: A/an, some/any

A/an = one + исчисляемое (an orange, a pear)  
Some/any = некоторое количество + исчисляемое/неисчисляемое (any pears, some milk)  
Some - для утверждений  
Any - для вопросов и отрицаний  
I have an orange  
We have some fruit  
And do your friends have any milk?  
Mia doesn't have any oranges

### Количество: Much/many/a lot of

Much / many / a lot of = много  
a lot of + исчисляемое/неисчисляемое для утверждений  
much + неисчисляемое для вопросов и отрицаний  
many + исчисляемое для вопросов и отрицаний  
How many + исчисляемое / How much + неисчисляемое = Сколько?  
We have a lot of vegetables  
But we don't have much juice and many nuts  
And you? How much juice do you have?  
So, how many nuts do you have? - I have a lot of nuts

![To Have Present: negation and question](https://bytebucket.org/ilyaboltarev/info/raw/5045a38f9f5cf12b5e131a9a3bb17e9c0ad18a9c/English/images/01-06.jpg)

# 07. Home sweet home

## Dictionary

dining room - столовая  
study - кабинет  
sofa - диван  
cupboard - буфет  
basin - умывальник  
sink - раковина  
oven - духовка

## Theory

### There is / are

There is/are - там есть  
There is + один  
There are + больше, чем один  
There are a lot of trees there  
There is a nice garden near his house

### There is / are not

There is/are + not - там нет  
There is (not) + water/milk/time = то, что нельзя посчитать  
There is not a pool at his house  
There are not many flowers at his garden

Если предмет неисчисляемый, то используем there is  
There is a lot of grass in his garden

### Is there / Are there

Is/Are there? -Yes, there is/are  
Is/Are there? - No, there is/are not  
There is not = There isn't  
There are not = There aren't  
Is there a pool at you house? - Yes, there is.  
Are there many flowers in this garden? - Yes, there are

### Один - больше, чем один - много

A/an = один  
Some/any = больше одного  
Much/many/a lot of = много  
Some/a lot of - для утверждений  
Any/much/many - для вопросов и отрицаний  
There is a window in my room  
There are some windows in his room  
There are a lot of windows in my parents' room  
Are there any windows in your room? - No, there are not any  
Форма зависит от слова (числа этого слова), которое идет первым после оборота  
There is not much water in the pool and many chairs around it (в бассейне нет много воды и нет много стульев вокруг него)

![There is/are: Present: affirmative, negation and question](https://bytebucket.org/ilyaboltarev/info/raw/5045a38f9f5cf12b5e131a9a3bb17e9c0ad18a9c/English/images/01-07.jpg)