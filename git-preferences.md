```
git config --global user.name "Your Name"
git config --global user.email "your_email@whatever.com"
```

```
git config --global color.ui true
```

For Unix/Mac users:
```
git config --global core.autocrlf input
git config --global core.safecrlf true
```

For Windows users:
```
git config --global core.autocrlf true
git config --global core.safecrlf true
```