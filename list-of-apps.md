# macOS Apps
- 1Password (+ Safari Extension)
- iA Writer
- Dropbox
- 2Do
- battle.net
- Google Chrome Browser
- Last.fm
- AppCleaner
- VLC Media Player
- uTorrent
- Skype

# macOS Dev Apps
- Xcode
- Xcode Command Line Tools (```xcode-select --install```)
- [Forklift](http://www.binarynights.com/forklift/)
- [Sublime Text 3](https://www.sublimetext.com)
- [Sublime Text 3 Preferences](https://bitbucket.org/ilyaboltarev/info/src/8fdff2cf6bb0af412ac534d2213a3fc3b0fecb03/sublime-text-preferences.md)
- Sites/ folder in user's folder
- [Git Preferences](https://bitbucket.org/ilyaboltarev/info/src/8b2d2824de7bd5946fff33ca9f1e447cec7dcd11/git-preferences.md)
- [iTerm2](https://www.iterm2.com)
- [iTerm2 Preferences](https://bitbucket.org/ilyaboltarev/info/src/8fdff2cf6bb0af412ac534d2213a3fc3b0fecb03/iterm2-preferences.md)
- [PHP, phpMyAdmin, MySQL and Apache](https://coolestguidesontheplanet.com/install-apache-mysql-php-and-phpmyadmin-on-macos-high-sierra-10-13/)
- [Codekit app](https://codekitapp.com)
- [Tower](https://www.git-tower.com/mac/)
- [Dash](https://kapeli.com/dash)

# iPad Apps
- 1Password
- iA Writer
- 2Do

# iPad Games
- Empires & Puzzles