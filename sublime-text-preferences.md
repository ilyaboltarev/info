## Installation and Base Settings

Настройки Sublime’а хранятся в Sublime Text - Preferences - Settings. Все изменения настроек записываем в Preferences.sublime-settings - User.
```
{
  "font_size": 20,
  "font_face": "PT Mono",
  "word_wrap": true,
  "draw_minimap_border": true,
  "always_show_minimap_viewport": true,
  "scroll_past_end": true,
  "theme": "Default.sublime-theme",
  "close_windows_when_empty": false,
  "tab_size": 2,
  "translate_tabs_to_spaces": true
}
```

## Multiple Cursors

В Sublime Text 3 можно выделить какое-нибудь слово и нажимая ⌘D выделять другие такие же слова. На них будут стоять multiple cursors и поэтому переименовать их можно будет разом. Если слово не выделено, а курсор на нем стоит, то нажав ⌘D в первый раз - это слово будет выделено.

⌃⌘G - выделит сразу все похожие слова в документе и поставит на них multiple cursors

Также можно зажать ⌥ и выделять строки. На всех этих строках будут стоять multiple cursors

## The Command Palette

⇧⌘P - открываем Command Palette

Здесь мы можем поменять синтаксис документа, например. Команда Set Syntax. Но нам не обязательно писать команду полностью, достаточно написать название нужного синтаксиса и из предложенного в меню выбрать нужное.

Также в Command Palette можно прятать или показывать сайдбар, прятать или показывать миникарту и др.

## Instant File Changing

Допустим у меня очень большой проект в котором много папок и файлов. Я хочу найти конкретный файл. Чтоб не лазать по всей структуре проекта, я могу вызвать Goto Anything через ⌘P и прописать там название файла. В меню покажут все файлы с таким именем и путь к ним.

## Symbols

Зайдем в какой-нибудь файл, где есть много методов (классов). Нажмем ⌘R и будет показан список всех классов в этом файле. Также этот список мы можем вызвать через ⌘P и в строке поиска поставить знак @

В файле .css если нажмем ⌘R - будет показан список всех селекторов.

Например, у меня открыт .php-файл, а я хочу поменять fieldset в файле style.css. Я нажимаю ⌘P и пишу style.css@fieldset - сразу попадаю на селектор fieldset в style.css

## Key Bindings

Список всех горячих клавиш можно посмотреть в Sublime Text - Preferences - Key Bindings. Все изменения мы должны вносить в Default (OSX).sublime-keymap - User

## Reindent

С помощью Reindent мы можем привести наш код в нормальный вид (сделать нормальные отступы)

В Key Bindings User прописываем:
```
[
  { "keys": ["super+alt+r"], "command": "reindent" }
]
```

Теперь если у кода неправильные отступы (съехали), выделяем весь код и нажимаем ⌘⌥R

## Package Control

Заходим в Command Palette и набираем: Install Package Control.
Также можно установить, вставив код с сайта [https://packagecontrol.io](https://packagecontrol.io) в консоль (⌃`)

## Устанавливаем свой Color Scheme

1. Open Sublime text and click on
Preferences -> Browse Packages
2. Once you are inside the Sublime Text folder called "Packages" create a new folder naming it "Colorsublime-Themes"
3. Then put the file/files .tmTheme inside the "Colorsublime-Themes" folder
4. Cool! Now you should be able to select your custom themes by browsing
Preferences -> Color Scheme -> Colorsublime-Themes

## Полезные плагины
1. Theme - SodaReloaded (в Preferences.sublime-settings - User пишем: `"theme": "SoDaReloaded Dark.sublime-theme"` или `"theme": "SoDaReloaded Light.sublime-theme"`)
2. Emmet (в Preferences - Package Settings - Emmet - Settings (User): `{"insert_final_tabstop": true}`)
3. AdvancedNewFile (новые файлы в проекте можно создавать через ⌥⌘N)
4. SidebarEnhancements (больше функций в меню сайдбара)
5. Gist (в Github - Settings - Personal Access Token нажимаем Generate a Personal Access Token, даём название, убираем все галочки кроме Gist, нажимаем Generate token, копируем токен. В Sublime Text - Preferences - Package Settings - Gist - Settings (User) вставляем токен: `{ "token": " "}`)
6. BracketHighlighter (подсвечивает открывающий и закрывающий теги)
7. AutoFileName (при вставке в код адресов к css-файлам, картинкам и т.д. сам предлагает названия файлов из тех, что есть в папке проекта)
8. Color Highlighter (показывает цвета при выделении кода цвета)
9. Goto-CSS-Declaration (в html выделяем нужный класс, жмем правой кнопкой мыши, выбираем Goto CSS Declaration и сразу попадаем в нужное место css-файла)
10. Docblockr (в Sublime Text - Preferences - Package Settings - Docblockr - Settings (User): `{"jsdocs_extend_double_slash": false}`)
11. DashDoc
12. Jade
13. Stylus
14. Sass
15. SCSS
16. Highlight Whitespaces (подсвечивает отступы)
17. SublimeLinter
18. SublimeLinter-php
19. Pug
20. Better Coffeescript
21. Plain Tasks [(инструкция)](https://bitbucket.org/ilyaboltarev/info/src/8fdff2cf6bb0af412ac534d2213a3fc3b0fecb03/plaintasks.md)
22. CDNJS
23. Random Everything
24. HTML-CSS-JS Prettify
25. Git
26. One Dark Material Theme
27. One Dark Color Scheme
28. CSScomb

[Поиск плагинов](https://packagecontrol.io/search)